package com.cablemc.pokemoncobbled.fabric.mixin;

import com.cablemc.pokemoncobbled.common.client.PokemonCobbledClient;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.client.util.math.MatrixStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * Mixin to give us a hook to render the PartyOverlay below the Chat
 *
 * @author Qu
 * @since 2022-02-22
 */
@Mixin(InGameHud.class)
public class GuiMixin {
    @Inject(
            method = "render",
            at = @At(
                value = "INVOKE",
                target = "Lnet/minecraft/client/util/math/MatrixStack;push()V",
                shift = At.Shift.BEFORE
            ),
            slice = @Slice(
                from = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/hud/InGameHud;renderScoreboardSidebar(Lnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/scoreboard/ScoreboardObjective;)V")
            )
    )
    private void beforeChatHook(MatrixStack poseStack, float f, CallbackInfo ci) {
        PokemonCobbledClient.INSTANCE.beforeChatRender(poseStack, f);
    }
}
