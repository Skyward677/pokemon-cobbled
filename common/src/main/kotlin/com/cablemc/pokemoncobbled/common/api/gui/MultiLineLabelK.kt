package com.cablemc.pokemoncobbled.common.api.gui

import net.minecraft.client.MinecraftClient
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.text.StringVisitable
import net.minecraft.text.Style
import net.minecraft.text.Text
import net.minecraft.util.Identifier
import java.util.stream.Collectors

class MultiLineLabelK(
    private val comps: List<TextWithWidth>,
    private val font: Identifier? = null
) {

    companion object {
        private val mcFont = MinecraftClient.getInstance().textRenderer

        fun create(component: Text, width: Number, maxLines: Number) = create(component, width, maxLines, null)

        fun create(component: Text, width: Number, maxLines: Number, font: Identifier?): MultiLineLabelK {
            return MultiLineLabelK(
                mcFont.textHandler.wrapLines(component, width.toInt(), Style.EMPTY).stream()
                    .limit(maxLines.toLong())
                    .map {
                    TextWithWidth(it, mcFont.getWidth(it))
                }.collect(Collectors.toList()),
                font = font
            )
        }
    }

    fun renderLeftAligned(
        poseStack: MatrixStack,
        x: Number, y: Number,
        ySpacing: Number,
        colour: Int,
        shadow: Boolean = true
    ) {
        comps.forEachIndexed { index, textWithWidth ->
            drawString(
                poseStack = poseStack,
                x = x, y = y.toFloat() + ySpacing.toFloat() * index,
                colour = colour,
                shadow = shadow,
                text = textWithWidth.text.string,
                font = font
            )
        }
    }

    class TextWithWidth internal constructor(val text: StringVisitable, val width: Int)
}