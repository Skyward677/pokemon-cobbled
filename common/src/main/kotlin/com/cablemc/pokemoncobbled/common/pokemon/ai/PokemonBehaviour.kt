package com.cablemc.pokemoncobbled.common.pokemon.ai

/**
 * Collection of all AI properties defineable at the species level of a Pokémon.
 *
 * @author Hiroku
 * @since July 15th, 2022
 */
open class PokemonBehaviour {
    val resting = RestBehaviour()
    var moving = MoveBehaviour()
}