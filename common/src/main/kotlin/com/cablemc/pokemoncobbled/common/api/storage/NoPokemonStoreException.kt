package com.cablemc.pokemoncobbled.common.api.storage

class NoPokemonStoreException(message: String) : Exception(message)