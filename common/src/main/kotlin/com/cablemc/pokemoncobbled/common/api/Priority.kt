package com.cablemc.pokemoncobbled.common.api

enum class Priority {
    HIGHEST,
    HIGH,
    NORMAL,
    LOW,
    LOWEST
}