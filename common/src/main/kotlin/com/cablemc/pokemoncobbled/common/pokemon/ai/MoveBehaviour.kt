package com.cablemc.pokemoncobbled.common.pokemon.ai

/**
 * Behavioural properties relating to a Pokémon's ability to look and move.
 *
 * @author Hiroku
 * @since July 30th, 2022
 */
class MoveBehaviour {
    val canMove = true
    val canLook = true
}