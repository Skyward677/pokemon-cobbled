package com.cablemc.pokemoncobbled.common.api.item

enum class Flavor {
    SPICY,
    DRY,
    SWEET,
    BITTER,
    SOUR
}