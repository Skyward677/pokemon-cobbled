package com.cablemc.pokemoncobbled.common.client.render.models.blockbench.frame

import net.minecraft.client.model.ModelPart

interface ModelFrame {
    val rootPart: ModelPart
}