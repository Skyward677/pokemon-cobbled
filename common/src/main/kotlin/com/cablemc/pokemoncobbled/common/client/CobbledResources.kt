package com.cablemc.pokemoncobbled.common.client

import com.cablemc.pokemoncobbled.common.util.cobbledResource
import net.minecraft.util.Identifier

object CobbledResources {
    /**
     * Textures
     */
    val RED = cobbledResource("textures/red.png")
    val WHITE = cobbledResource("textures/white.png")
    val PHASE_BEAM = cobbledResource("textures/phase_beam.png")

    /**
     * Fonts
     */
    val DEFAULT_LARGE = Identifier("uniform")
}